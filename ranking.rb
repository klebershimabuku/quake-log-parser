require 'pry'
require './player'

class Ranking

  def initialize(parser_results)
    @parser_results = parser_results
  end

  def generate
    @parser_results.each do |game|
      game_stats = game[1][:kills]

      game_stats.each do |player_name, kills|
        if (player = Player.find_by_name(player_name))
          player.add_kills(kills)
        else
          player = Player.new(name: player_name, kills: kills)
        end

        player.save
      end
    end

    RankingPlayers.build
  end
end
