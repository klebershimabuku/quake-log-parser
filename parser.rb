require './game_match'
require './formatter'
require './result'

class Parser

  def initialize(logfile)
    @logfile = logfile
    @events = []
    @game_matches = []
  end

  def parse
    line = File.open(@logfile, 'r').each_line do |line|
      data = Formatter.line(line)

      if data[:event].match(/InitGame/)
        if @game.nil?
          # do nothing
        elsif @game.matches.any?
          @game_matches << @game
        end

        @game = GameMatch.new
      end

      if data[:event].match(/Kill/)
        @game.save(data)
      end

      if data[:event].match(/ShutdownGame/)
        @game_matches << @game
        @game = GameMatch.new
      end
    end

    Result.to_hash(@game_matches)
  end

end
