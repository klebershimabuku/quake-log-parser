require 'pry'

class Result

  def self.to_hash(games)
    new(games).to_hash
  end

  def initialize(games)
    @games = games
  end

  def to_hash
    index = 1
    @result = {}

    @games.each do |game|
      if game.matches.any?
        @result.merge!(
          :"game_#{index}" => {
          total_kills: game.matches.size,
          players: players(game),
          kills: build_kills_hash(game)
        }
        )
        index += 1
      end
    end

    @result
  end

  protected
  def players(game)
    players = []
    game.matches.map do |match|
      players << match.select { |k,_| k == :killer || k == :victim }.values
    end.flatten.uniq.delete_if { |e| e == '<world>' }
  end

  def build_kills_hash(game)
    hash = {}

    players(game).each do |player|
      hash.merge!(player => kills_by_player(game, player))
    end

    hash.merge!(kills_by_means(game))
  end

  def kills_by_means(game)
  end

  def kills_by_player(game, player)
    kills = 0

    game.matches.each do |match|
      kills += 1 if match[:killer] == player
      kills -= 1 if match[:killer] == '<world>' && match[:victim] == player
    end

    kills
  end

end
