require './ranking_players'

class Player
  include RankingPlayers
  attr_accessor :name, :kills

  def initialize(options)
    @name = options[:name]
    @kills = options[:kills]
  end

  def self.find_by_name(name)
    if (player = RankingPlayers.exists?(name))
      player
    else
      false
    end
  end

  def add_kills(kills)
    self.kills += kills
  end

  def save
    RankingPlayers.add(self)
  end

  def to_hash
    { "#{self.name}" => self.kills }
  end

end
