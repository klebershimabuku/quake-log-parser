module RankingPlayers
  def self.players
    @players ||= []
  end

  def self.add(player)
    if players.map(&:name).include?(player.name)
      players.delete_if { |p| p.name == player.name }
    end

    players << player
  end

  def self.exists?(player_name)
    if players.map(&:name).include?(player_name)
      kills = players.detect { |player| player.name == player_name }.kills
      Player.new(name: player_name, kills: kills)
    else
      false
    end
  end

  def self.build
    hash = {}

    players.each do |player|
      hash.merge!(player.to_hash)
    end

    hash
  end

  def self.clear
    @players = []
  end
end
