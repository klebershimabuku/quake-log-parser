#!/usr/bin/env ruby
require './parser'
require './ranking'

parser = Parser.new ARGV[0]
pp parser.parse

ranking = Ranking.new(parser.parse)
puts '---'
puts 'generating ranking..'
puts '---'
pp ranking.generate
