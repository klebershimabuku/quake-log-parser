class Formatter
  def self.line(line)
    data = line.split(' ')
    time = data[0]
    event = data[1]

    participants = line.match(/:.+?:.+?:\s*(.+?) killed (.+?) by (.+)/)
    killer = participants[1] rescue nil
    victim = participants[2] rescue nil
    mean = participants[3] rescue nil

    {
      :time => time,
      :event => event,
      :killer => killer,
      :victim => victim,
      :mean => mean
    }
  end
end
