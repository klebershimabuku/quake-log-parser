require './ranking'

describe Ranking do
  let(:parser_results) do
    {
      :game_1 => {
        :total_kills => 11,
        :players => [
          "Isgalamido",
          "Mocinha"
        ],
        :kills => {
          "Isgalamido"=>-5,
          "Mocinha"=>0
        }
      },
     :game_2 => {
        :total_kills=>4,
        :players => [
          "Isgalamido",
          "Mocinha",
          "Zeh",
          "Dono da Bola"
        ],
        :kills => {
          "Isgalamido"=>1,
          "Mocinha"=>0,
          "Zeh"=>-2,
          "Dono da Bola"=>-1
        }
      },
     :game_3 => {
        :total_kills => 105,
        :players => [
          "Isgalamido",
          "Dono da Bola",
          "Zeh",
          "Assasinu Credi"
        ],
        :kills => {
          "Isgalamido"=>19,
          "Dono da Bola"=>13,
          "Zeh"=>20,
          "Assasinu Credi"=>13
        }
      }
    }
  end

  let(:ranking) { Ranking.new(parser_results) }

  context 'generates' do
    let(:results) { ranking.generate }

    it 'sums the kills per player' do
      expect(results['Isgalamido']).to eq(15)
      expect(results['Mocinha']).to eq(0)
      expect(results['Zeh']).to eq(18)
      expect(results['Dono da Bola']).to eq(12)
      expect(results['Assasinu Credi']).to eq(13)
    end

    it 'returns the right type' do
      expect(results).to be_a(Hash)
    end
  end
end
