require './result'

describe Result do

  let(:game_match_0) { double('fake_game_match') }
  let(:game_match_1) { double('fake_game_match') }

  let(:games) { [game_match_0, game_match_1] }

  let(:result) { Result.to_hash(games) }

  context 'per game' do

    before do
      allow(game_match_0).to receive(:matches).and_return(
          [
            { :time=>"20:54", :event=>"Kill:", :killer=>"<world>", :victim=>"Isgalamido", :mean=>"MOD_TRIGGER_HURT"},
            { :time=>"21:07", :event=>"Kill:", :killer=>"<world>", :victim=>"Isgalamido", :mean=>"MOD_TRIGGER_HURT"},
            { :time=>"21:42", :event=>"Kill:", :killer=>"<world>", :victim=>"Isgalamido", :mean=>"MOD_TRIGGER_HURT"},
            { :time=>"22:06", :event=>"Kill:", :killer=>"Isgalamido", :victim=>"Mocinha", :mean=>"MOD_ROCKET_SPLASH"}
          ]
      )
      allow(game_match_1).to receive(:matches).and_return(
          [
            { :time=>"2:00", :event=>"Kill:", :killer=>"<world>", :victim=>"Isgalamido", :mean=>"MOD_TRIGGER_HURT"},
            { :time=>"2:04", :event=>"Kill:", :killer=>"<world>", :victim=>"Dono da Bola", :mean=>"MOD_FALLING"},
            { :time=>"2:04", :event=>"Kill:", :killer=>"<world>", :victim=>"Isgalamido", :mean=>"MOD_FALLING"},
            { :time=>"2:11", :event=>"Kill:", :killer=>"Dono da Bola", :victim=>"Zeh", :mean=>"MOD_ROCKET"},
            { :time=>"2:22", :event=>"Kill:", :killer=>"Isgalamido", :victim=>"Dono da Bola", :mean=>"MOD_RAILGUN"},
            { :time=>"2:29", :event=>"Kill:", :killer=>"Isgalamido", :victim=>"Zeh", :mean=>"MOD_RAILGUN"}
          ]
      )
    end

    it { expect(result).to be_an(Hash) }
    it { expect(result[:game_1]).to be_an(Hash) }
    it { expect(result[:game_2]).to be_an(Hash) }

    it 'total_kills' do
      expect(result[:game_1][:total_kills]).to eq(4)
      expect(result[:game_2][:total_kills]).to eq(6)
    end

    it 'players' do
      expect(result[:game_1][:players]).to be_an(Array)
      expect(result[:game_1][:players]).to include('Mocinha', 'Isgalamido')
      expect(result[:game_1][:players]).to_not include('<world>')
      expect(result[:game_2][:players]).to include('Isgalamido', 'Zeh', 'Dono da Bola')
    end

    context 'kills per player' do
      it { expect(result[:game_1][:kills]).to be_a(Hash) }
      it { expect(result[:game_1][:kills]['Isgalamido']).to eq(-2) }
      it { expect(result[:game_1][:kills]['Mocinha']).to eq(0) }

      it { expect(result[:game_2][:kills]['Isgalamido']).to eq(0) }
      it { expect(result[:game_2][:kills]['Dono da Bola']).to eq(0) }
      it { expect(result[:game_2][:kills]['Zeh']).to eq(0) }
    end

    context 'kills by means' do
      it { expect(result[:game_1][:kills][:means]).to be_a(Hash) }
      it { expect(result[:game_1][:kills][:means]['MOD_TRIGGER_HURT']).to eq(3) }
      it { expect(result[:game_1][:kills][:means]['MOD_ROCKET_SPLASH']).to eq(1) }

      it { expect(result[:game_2][:kills][:means]['MOD_TRIGGER_HURT']).to eq(1) }
      it { expect(result[:game_2][:kills][:means]['MOD_FALLING']).to eq(2) }
      it { expect(result[:game_2][:kills][:means]['MOD_ROCKET']).to eq(1) }
      it { expect(result[:game_2][:kills][:means]['MOD_RAILGUN']).to eq(2) }
    end
  end
end
