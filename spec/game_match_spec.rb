require './game_match'

describe GameMatch do
  describe "#initialize" do
    it "returns an array" do
      gm = GameMatch.new
      expect(gm.matches).to be_an(Array)
    end

    it "returns an empty array of matches" do
      gm = GameMatch.new
      expect(gm.matches).to be_empty
    end
  end

  describe "#save" do
    it "appends data to the matches" do
      gm = GameMatch.new
      expect(gm.matches).to be_empty

      gm.save('foo')

      expect(gm.matches).to_not be_empty
      expect(gm.matches).to include('foo')
    end
  end
end
