require 'pry'
require './player'
require './ranking_players'

describe Player do
  include RankingPlayers

  let(:player) { Player.new(name: 'foo', kills: 5) }

  describe ".find_by_name" do
    let!(:players) { player.save }

    it 'when found returns the player instance' do
      p = Player.find_by_name('foo')
      expect(p).to be_an(Player)
    end

    it 'when not found return false' do
      p = Player.find_by_name('bar')
      expect(p).to eq(false)
    end
  end

  describe "#initialize" do
    it "creates a new player instance" do
      expect(player).to be_an(Player)
    end

    it "player has a name" do
      expect(player.name).to eq('foo')
    end

    it "player has a number of kills" do
      expect(player.kills).to eq(5)
    end
  end

  describe "#add_kills" do
    it 'increments the number of kills' do
      player.add_kills(2)
      expect(player.kills).to eq(7)
    end
  end

  describe "#save" do
    let(:players) { RankingPlayers.players }

    it "saves with success" do
      player2 = Player.new(name: 'bar', kills: 0)

      player2.save

      expect(players.size).to eq(2)
      expect(players[1].name).to eq('bar')
    end
  end

  describe "#to_hash" do
    it { expect(player.to_hash).to be_an(Hash) }

    it "has name" do
      hash = player.to_hash
      expect(hash).to have_key(player.name)
    end

    it "has kills" do
      hash = player.to_hash
      expect(hash).to have_value(player.kills)
    end
  end
end
