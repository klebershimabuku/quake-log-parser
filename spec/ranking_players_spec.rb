require './ranking_players'
require './player'

describe RankingPlayers do

  describe ".players" do
    before { RankingPlayers.clear }

    let(:players) { RankingPlayers.players }

    it "when not exists, returns empty array" do
      expect(players).to be_an(Array)
      expect(players).to be_empty
    end
    it "when exists any, returns the players" do
      player = Player.new(name: 'foo', kills: 1)

      RankingPlayers.add(player)

      expect(players.size).to eq(1)
      expect(players).to include(player)
    end
  end

  describe ".add" do
    pending('tested on the previous example')
  end

  describe".exists?" do
    it "when exists returns a new player instance" do
      rp = RankingPlayers.exists?('foo')
      expect(rp).to be_an(Player)
    end

    it "when not exists, returns false" do
      rp = RankingPlayers.exists?('bar')
      expect(rp).to eq(false)
    end
  end

  describe ".build" do
    it "when exists players" do
      player = Player.new(name: 'foo', kills: 10)

      expect(RankingPlayers).to receive(:players).twice { [player] }
      expect(RankingPlayers.build).to be_an(Hash)
      expect(RankingPlayers.build).to include('foo')
    end

    it "returns a empty hash when exists no players" do
      expect(RankingPlayers).to receive(:players) { [] }
      expect(RankingPlayers.build).to eq({})
    end
  end

  describe ".clear" do
    it "clears the players list" do
      player = Player.new(name: 'foo', kills: 10)
      RankingPlayers.add(player)

      expect(RankingPlayers.players).to_not be_empty

      RankingPlayers.clear

      expect(RankingPlayers.players).to be_empty
    end
  end
end
