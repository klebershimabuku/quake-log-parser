require './formatter'
describe Formatter do
  let(:line) do
    "1:41 Kill: 1022 2 19: <world> killed Dono da Bola by MOD_FALLING"
  end

  let(:f) { Formatter.line(line) }

  it "returns a hash instance" do
    expect(f).to be_an(Hash)
  end

  context 'extracts the right data' do
    it { expect(f[:time]).to eq('1:41') }
    it { expect(f[:event]).to eq('Kill:') }
    it { expect(f[:killer]).to eq('<world>') }
    it { expect(f[:victim]).to eq('Dono da Bola') }
    it { expect(f[:mean]).to eq('MOD_FALLING') }
  end
end
