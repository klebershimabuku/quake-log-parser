class GameMatch
  attr_accessor :matches

  def initialize
    @matches = []
  end

  def save(data)
    @matches << data
  end
end
